# json

## Introduction

This is a simple json parser/dumper for squirrel made to use on [Gothic 2 Online](https://gothic-online.com/) platform.  
The json library uses [nlohmann::json](https://github.com/nlohmann/json) project as a backend for implementing this feature.

## Documentation

https://gothicmultiplayerteam.gitlab.io/modules/json

### Example code:
```lua
addEventHandler("onInit", function()
{
    system("chcp 1250")
    
    local from_container = {a = 1.5, b = "zażółć gąślą jaźń", c = false, d = null, e = {f = [1, 2, [3, 4]]}}

    local json_string = JSON.dump_ansi(from_container, 2)

    local write = file("test.json", "w+")
    write.write(json_string)
    write.close()

    local read = file("test.json", "r+")
    local to_container = JSON.parse_ansi(read.read("a"))
    read.close()

    print(to_container["b"])
})
```