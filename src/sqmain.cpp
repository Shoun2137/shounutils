#include <sqapi.h>
#include "json.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

	/* squirreldoc (class)
	*
	* This class represents JSON utilities.
	*
	* @side		shared
	* @name		JSON
	*
	*/
	Sqrat::RootTable(vm).Bind("JSON", Sqrat::Table(vm)
		/* squirreldoc (method)
		*
		* This method will dump json as a text encoded via utf8.
		*
		* @name     dump_utf8
		* @param    (array|table) value the value that will be dumped as json.
		* @param    (int) indent=-1 the number that reperesents how many times indentation character will be placed.
		* @param    (int) indent_chat=' ' the indentation character.
		* @param    (bool) ensure_ascii=false If ensure_ascii is true, all non-ASCII characters in the output are escaped with \uXXXX sequences, and the result consists of ASCII characters only.
		* @return	(string) the dumped json as text.
		*
		*/
		.SquirrelFunc("dump_utf8", &JSON::dump_utf8, -2, "..iib")
		/* squirreldoc (method)
		*
		* This method will dump json as a text encoded via ansi.
		*
		* @name     dump_ansi
		* @param    (array|table) value the value that will be dumped as json.
		* @param    (int) indent=-1 the number that reperesents how many times indentation character will be placed.
		* @param    (int) indent_chat=' ' the indentation character.
		* @param    (bool) ensure_ascii=false If ensure_ascii is true, all non-ASCII characters in the output are escaped with \uXXXX sequences, and the result consists of ASCII characters only.
		* @return	(string) the dumped json as text.
		*
		*/
		.SquirrelFunc("dump_ansi", &JSON::dump_ansi, -2, "..iib")

		/* squirreldoc (method)
		*
		* This method will parse json text encoded via utf8 and returne the appropiate object.
		*
		* @name     parse_utf8
		* @param    (string) text the text that you want to parse.
		* @return	(array|table) the parsed object.
		*
		*/
		.SquirrelFunc("parse_utf8", &JSON::parse_utf8, 2, ".s")

		/* squirreldoc (method)
		*
		* This method will parse json text encoded via ansi and returne the appropiate object.
		*
		* @name     parse_ansi
		* @param    (string) text the text that you want to parse.
		* @return	(array|table) the parsed object.
		*
		*/
		.SquirrelFunc("parse_ansi", &JSON::parse_ansi, 2, ".s")
	);

	return SQ_OK;
}
